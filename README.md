
# Stable Matching Lattice

For a specified number of men and women and a random set of preferences of each individual over the members of the other sex, we generate all possible stable matchings and compute the lattice of these matchings.


## Background

Suppose that you have N single men and N single women, each with a set of preferences over the members of the opposite sex. A matching is a pairing up of each man with exactly one woman, i.e. a bijection from the men to the women. A matching is called <strong>stable</strong> if there’s no unmatched pair of individuals who both prefer each other over their partner in the matching.

One can prove that there is always a stable matching. Indeed there is often more than one stable matching. The various matchings can be ordered by Pareto improvements from the perspective of one sex. For instance, suppose we choose to consider the perspective of the men. Matching M1 is said to be superior to matching M2 if every man is at least as happy with his match in M1 as he is with his match in M2. The set of stable matchings ordered in this way forms a [distributive lattice](https://en.wikipedia.org/wiki/Distributive_lattice/). In fact, something stronger is true: EVERY distributive lattice can be thought of as the set of stable matchings for some set of individuals, ordered by Pareto improvements!

In this program, the user specifies the number of men and women. Then a random set of preferences is generated for each individual and all stable matchings are generated. Finally the ordering on the matchings is computed.

## Examples

![](Media/stable-matching-lattice-example-1.png)

![](Media/stable-matching-lattice-example-2.png)

