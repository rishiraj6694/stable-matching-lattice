from itertools import permutations
from itertools import product
from random import choice

def stableMatchings(men,women):
	stableMatchings = []
	for matching in matchings:
		if isStable(matching, men, women):
			stableMatchings.append(matching)
	return stableMatchings

def isStable(matching, men, women):
	'''
	e.g. matching = [0,2,1]
	m = [(0,0), (1,2), (2,1)]
	corresponds to:
		0 -> 0
		1 -> 2
		2 -> 1
	To check stability, we look for blocking pairs
	Check (0,1), (0,2), (1,0), (1,1), (2,0), (2,1)
	'''
	toCheck = list(permutations(indices, 2))
	toCheck = list(product(indices, repeat=2))
	m = [(i, matching[i]) for i in range(n)]

	for pair in toCheck:
		if pair not in m:
			if isBlockingPair(pair, matching, men, women):
				return False
	return True

def isBlockingPair(pair, matching, men, women):
	'''
	eg. pair = (2,1)
		man = 2
		woman = 1
		matching = [0,2,1]
		m = [(0,0), (1,2), (2,1)]
	Does man 2 prefer woman 1 over matching[2]?
		men[2].index(1) < men[2].index(matching[2]) ?
	Does woman 1 prefer man 2 over her match, matching.index(1)?
		women[1].index(2) < women[1].index(matching.index(1))?
	If both return True
	'''
	man,woman = pair
	if men[man].index(woman) < men[man].index(matching[man]):
		if women[woman].index(man) < women[woman].index(matching.index(woman)):
			return True
	return False

def isParetoImprovement(matching1, matching2, men, women):
	'''
	matching1 = [1,0,2]
		0 1
		1 0
		2 2
	matching2 = [2,0,1]
		0 2
		1 0
		2 1
	See if all men would be happier in matching1 than matching2
		man = 0
			men[0].index(1) >= men[0].index(2)
		man = 1
			men[1].index(0) >= men[1].index(0)
		man = 2
			men[2].index(2) >= men[2].index(1)
	'''
	for man in range(n):
		partner1 = matching1[man]
		partner2 = matching2[man]
		if men[man].index(partner1) > men[man].index(partner2):
			return False
	return True

def getLattice(stable_matchings):
	num_stable = len(stable_matchings)
	order = [["-" for i in range(num_stable)] for j in range(num_stable)]
	for i in range(num_stable):
		for j in range(num_stable):
			matching1 = stable_matchings[i]
			matching2 = stable_matchings[j]
			if isParetoImprovement(matching1, matching2, men, women):
				order[i][j] = "T"
			elif isParetoImprovement(matching2, matching1, men, women):
				order[i][j] = "F"
	return order
	print("Ordering")
	prettyPrintOrder(order)

def prettyPrintPreferences(preferences):
	for i in range(n):
		iPref = [str(k) for k in preferences[i]]
		print("\t", i, ":", " > ".join(iPref))

def prettyPrintMatching(matching):
	for i in range(n):
		print(f"\t\tMan {i} with woman {matching[i]}")

def prettyPrintLattice(order):
	indices = [str(i) for i in range(len(order))]
	print("\t ≥", " ".join(indices))
	for row in order:
		print("\t", order.index(row), end=" ")
		for val in row:
			print(val, end=" ")
		print()

while True:

	n = int(input("How many couples? "))

	indices = [i for i in range(n)]
	matchings = list(permutations(indices))

	men = [choice(matchings) for i in range(n)]
	women = [choice(matchings) for i in range(n)]
		## For instance
		## men[0][0] is man 0's first choice
		## women[2][1] is woman 2's second choice

	stable_matchings = stableMatchings(men,women)

	print("Men")
	prettyPrintPreferences(men)
	print("Women")
	prettyPrintPreferences(women)
	print()

	print("Stable Matchings")
	for matching in stable_matchings:
		print("\tMatching", stable_matchings.index(matching))
		prettyPrintMatching(matching)
	print()

	lattice = getLattice(stable_matchings)
	print("Lattice")
	prettyPrintLattice(lattice)
	print()

